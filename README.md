
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelsarthur

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelsarthur is to …

## Installation

You can install the development version of squirrelsarthur like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelsarthur)
## basic example code
```

Ajouter le texte que vous souhaitez pour presenter votre package a vos
utilisateurs.

``` r
get_message_fur_color(primary_fur_color = "black")
#> We will focus on black squirrels
```
